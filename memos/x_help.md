![Alt text](x_monty.png)

|Use See icon to back to your script|

##*What does SeePy is ?*

SeePy is a tool to create interactive report for scientific calculation you made with python script.

The mission of the project is to provide a simple and practical tool that will interest engineers to use Python language in 
their daily work.

You can use SeePy to create reports ready for publication. SeePy use *.py file format.
All additional SeePy syntax is hidden in ''' .... ''' or behind # so the python engine does not see it, this is still python standard code file you can execute. 
SeePy comments is based on easy to use Markdown language syntax.

##*Let's start!*

... just run Tutorial from Help menu to see how does it work.

